1) Run Changes.sql AFTER all of your other tables are installed. Running it before will cause errors.
2) Make sure to check that all the STBs in your client are updated to match the ones in this repo.
3) DO NOT extract STBs from the client to the server. Many important functions REQUIRE the STBs that are already in the server.